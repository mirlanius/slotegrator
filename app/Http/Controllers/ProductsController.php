<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductsController extends Controller
{    
    public function index() {
        return view('products.list');
    }
}
