<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class FavoritesController extends Controller
{
    public function show() {
       
        Gate::authorize('can-view');

        return view('products.favorites');
    }
}
