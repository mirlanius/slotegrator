<!-- Single Product -->
<div class="col-md-6 col-lg-4 col-xl-3">
        <div id="product-1" class="single-product">
                <div class="part-1">
                        <ul>
                                @auth
                                <li><a href="#"><i class="fas fa-shopping-cart"></i></a></li>
                                <li><a href="#"><i class="fas fa-heart"></i></a></li>
                                @endauth
                        </ul>
                </div>
                <div class="part-2">
                        <h3 class="product-title">Here Product Title</h3>
                        <h4 class="product-price">$49.99</h4>
                </div>
        </div>
</div>