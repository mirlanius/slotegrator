<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="UTF-8">
	<title>Mirlan PHP Dev</title>
	<link rel='stylesheet' href='/bootstrap.min.css'>
	<link rel='stylesheet' href='/all.min.css'>
	<link rel="stylesheet" href="/style.css">

	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css'>
		
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
<!-- partial:index.partial.html -->
<div class="container">
<header>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
	<a href="/"><img src="/logo.png" width="30" height="30" class="d-inline-block align-top" alt=""></a>
	<h5 class="my-0 mr-md-auto font-weight-normal">Slotegrator demo</h5>
	<nav class="my-2 my-md-0 mr-md-3">
	
		@auth 	<a class="p-2 text-dark" href="/favorites">Favorites</a> @endauth
		@guest	<a class="p-2 text-dark" href="/login">Login</a> @endguest
		@auth <a class="p-2 text-dark" href="/logout">Logout</a> @endauth

	</nav>
      @guest <a class="btn btn-outline-primary" href="/register">Sign up</a> @endguest
</div>

</header>

<section class="section-products">
	<div class="row">
		@yield('content')
	</div>
</section>

</div>
  
</body>
</html>
